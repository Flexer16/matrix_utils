---
-- view_factory.lua


local class = require "middleclass"


local ViewFactory = class("ViewFactory")

function ViewFactory:initialize(data)
    self.tile_size = 16
    self.quads = {}

    self.image = love.graphics.newImage("res/tiles/matrix_utils_tiles.png")
    self.image_width = self.image:getWidth()
    self.image_height = self.image:getHeight()

    self:_add_quad("?", 8, 8)

    self:_add_quad(".", 2, 1)
    self:_add_quad("s", 3, 1)
    self:_add_quad("e", 4, 1)
    self:_add_quad("x", 5, 1)

    self:_add_quad("cursor", 1, 1)
end

function ViewFactory:get_func(name)
    return function (name, x, y)
        if not self.quads[name] then
            -- error(name)

            name = "?"
        end

        love.graphics.draw(
            self.image,
            self.quads[name],
            x,
            y
        )
    end
end

function ViewFactory:_add_quad(name, x, y)
    self.quads[name] = love.graphics.newQuad(
        x * self.tile_size - self.tile_size,
        y * self.tile_size - self.tile_size,
        self.tile_size,
        self.tile_size,
        self.image_width,
        self.image_height
    )
end

return ViewFactory
