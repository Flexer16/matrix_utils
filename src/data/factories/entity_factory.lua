---
-- entity_factory.lua


local class = require "middleclass"

local ViewFactory = require "data.factories.view_factory"


local EntityFactory = class("EntityFactory")

function EntityFactory:initialize(data)
    self.view_factory = ViewFactory()
end

function EntityFactory:get_cell(x, y)
    local Position, View, Cell = Component.load({
        "Position", "View", "Cell"
    })

    local cell = Entity()
    cell:initialize()

    cell:add(Cell())
    cell:add(Position({x = x, y = y}))

    local view = View({tile = "."})
    view:set_draw_func(self.view_factory:get_func(view:get_tile()))
    cell:add(view)

    return cell
end

function EntityFactory:get_cursor()
    local Position, View, Cursor = Component.load({
        "Position", "View", "Cursor"
    })

    local cursor = Entity()
    cursor:initialize()

    cursor:add(Cursor())
    cursor:add(Position({x = 1, y = 1}))

    local view = View({tile = "cursor"})
    view:set_draw_func(self.view_factory:get_func(view:get_tile()))
    cursor:add(view)

    return cursor
end

return EntityFactory
