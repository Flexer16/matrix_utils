---
-- bresenham.lua


local class = require "middleclass"


local Bresenham = class("Bresenham")

function Bresenham:initialize(data)
    -- body
end

function Bresenham:line(i0, j0, i1, j1, funct)
    --получить значение для перестановок
    local steep = math.abs (j1 - j0) > math.abs (i1 - i0)

    --перестановки начала линии
    if steep then
        i0, j0 = j0, i0
        i1, j1 = j1, i1
    end

    local deltai
    local deltaj

    local inc

    --перестановки концов
    if i0 > i1 then
        --i0, i1 = i1, i0
        --j0, j1 = j1, j0

        deltai = i0 - i1
        deltaj = math.abs (j0 - j1)
        inc = -1
    else
        deltai = i1 - i0
        deltaj = math.abs (j1 - j0)
        inc = 1
    end

    local err = 0

    local tmpj = j0

    local jstep

    if j0 < j1 then
        jstep = 1
    else
        jstep = -1
    end

    for tmpi = i0, i1, inc do
        if steep then
            funct (tmpj, tmpi)
        else
            funct (tmpi, tmpj)
        end

        err = err + deltaj

        if (err * 2) >= deltai then
            tmpj = tmpj + jstep
            err = err - deltai
        end
    end
end

return Bresenham
