---
-- coordinate_converter.lua


local class = require "middleclass"


local CoordinateConverter = class("CoordinateConverter")

function CoordinateConverter:initialize(data)
    self.cell_size = data.cell_size
end

function CoordinateConverter:get_world_pos(local_pos)
    return (local_pos - 1) * self.cell_size
end

function CoordinateConverter:get_local_pos(world_pos)
    return math.floor(world_pos / self.cell_size + 1)
end

return CoordinateConverter
