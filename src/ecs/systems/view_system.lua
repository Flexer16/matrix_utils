---
-- view_system.lua


local class = require "middleclass"

local CoordinateConverter = require "utils.coordinate_converter"


local ViewSystem = class("ViewSystem", System)

function ViewSystem:initialize(data)
    System.initialize(self)

    self.tile_size = 16

    self.x_shift = 32
    self.y_shift = 32

    self.coord_converter = CoordinateConverter({cell_size = self.tile_size})
end

function ViewSystem:draw()
    self:_draw_map()
    self:_draw_cursor()
end

function ViewSystem:requires()
    return {}
end

function ViewSystem:onAddEntity(entity)
    -- body
end

function ViewSystem:onRemoveEntity(entity)
    -- body
end

function ViewSystem:_draw_map()
    local views = engine:getEntitiesWithComponent("Cell")

    for _, view in pairs(views) do
        local loc_x, loc_y = view:get("Position"):get()
        local glob_x = self.coord_converter:get_world_pos(loc_x)
        local glob_y = self.coord_converter:get_world_pos(loc_y)

        view:get("View"):draw_at(glob_x + self.x_shift, glob_y + self.y_shift)
    end
end

function ViewSystem:_draw_cursor()
    local cursors = engine:getEntitiesWithComponent("Cursor")

    for _, view in pairs(cursors) do
        local loc_x, loc_y = view:get("Position"):get()
        local glob_x = self.coord_converter:get_world_pos(loc_x)
        local glob_y = self.coord_converter:get_world_pos(loc_y)

        view:get("View"):draw_at(glob_x + self.x_shift, glob_y + self.y_shift)
    end
end

return ViewSystem
