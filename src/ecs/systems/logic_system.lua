---
-- logic_system.lua


local class = require "middleclass"

local Bresenham = require "utils.bresenham"


local LogicSystem = class("LogicSystem", System)

function LogicSystem:initialize(data)
    System.initialize(self)

    self.start = false
    self.finish = false

    self.start_point = nil
    self.end_point = nil

    signal:register("click-on-position", function (x, y) self:_get_click_position(x, y) end)
    signal:register("set-active", function (point) self:_set_active(point) end)
    signal:register("set-anactive", function (point) self:_set_anactive(point) end)

    self.bres = Bresenham()
    self.marcked_cells = {}
end

function LogicSystem:update(dt)
    -- body
end

function LogicSystem:requires()
    return {}
end

function LogicSystem:onAddEntity(entity)
    -- body
end

function LogicSystem:onRemoveEntity(entity)
    -- body
end

function LogicSystem:_get_click_position(x, y)
    if self.start then
        self:_set_start_cell(x, y)
    elseif self.finish then
        self:_set_end_cell(x, y)
    end

    if self.start_point and self.end_point then
        self:_calculate_bres_line()
    end
end

function LogicSystem:_set_active(point)
    if point == "start" then
        self.start = true
    elseif point == "end" then
        self.finish = true
    end
end

function LogicSystem:_set_anactive(point)
    if point == "start" then
        self.start = false
    elseif point == "end" then
        self.finish = false
    end
end

function LogicSystem:_set_start_cell(x, y)
    if self.start_point then
        self.start_point:get("Cell"):reset()
        self.start_point:get("View"):set_tile(".")
    end

    local start_cell = grid:get(x, y)

    if not start_cell:get("Cell"):is_end() then
        start_cell:get("View"):set_tile("s")
        self.start_point = start_cell
        self.start_point:get("Cell"):set_start()
    else
        self.start_point = nil
    end

    self:_clear_marcked_cells()
end

function LogicSystem:_set_end_cell(x, y)
    if self.end_point then
        self.end_point:get("Cell"):reset()
        self.end_point:get("View"):set_tile(".")
    end

    local end_cell = grid:get(x, y)

    if not end_cell:get("Cell"):is_start() then
        end_cell:get("View"):set_tile("e")
        self.end_point = end_cell
        self.end_point:get("Cell"):set_end()
    else
        self.end_point = nil
    end

    self:_clear_marcked_cells()
end

function LogicSystem:_calculate_bres_line()
    local start_x, start_y = self.start_point:get("Position"):get()
    local end_x, end_y = self.end_point:get("Position"):get()

    local bres_funct = function (x, y)
        local cell = grid:get(x, y)

        if not cell:get("Cell"):is_start() and not cell:get("Cell"):is_end() then
            cell:get("View"):set_tile("x")
            table.insert(self.marcked_cells, cell)
        end
    end

    self.bres:line(start_x, start_y, end_x, end_y, bres_funct)
end

function LogicSystem:_clear_marcked_cells()
    for _, cell in ipairs(self.marcked_cells) do
        cell:get("View"):set_tile(".")
    end

    self.marcked_cells = {}
end

return LogicSystem
