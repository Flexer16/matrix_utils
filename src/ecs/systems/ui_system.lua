---
-- ui_system.lua


local class = require "middleclass"
local Suit = require "suit"


local UiSystem = class("UiSystem", System)

function UiSystem:initialize(data)
    System.initialize(self)

    self.font = love.graphics.newFont("res/font/keyrusMedium.ttf", 18)

    self.button_width = 128
    self.button_height = 32

    self.ui = Suit.new()

    self.start_checkbox = {
        text = " Start",
        checked = false
    }

    self.end_checkbox = {
        text = " End",
        checked = false
    }
end

function UiSystem:update(dt)
    if self.ui:Checkbox(
        self.start_checkbox,
        {font = self.font},
        love.graphics.getWidth( ) - self.button_width, 0,
        self.button_width, self.button_height
    ).hit then
        if self.start_checkbox.checked then
            self.end_checkbox.checked = false
            signal:emit("set-active", "start")
            signal:emit("set-anactive", "end")
        else
            signal:emit("set-anactive", "start")
        end
    end

    if self.ui:Checkbox(
        self.end_checkbox,
        {font = self.font},
        love.graphics.getWidth( ) - self.button_width, self.button_height,
        self.button_width, self.button_height
    ).hit then
        if self.end_checkbox.checked then
            self.start_checkbox.checked = false
            signal:emit("set-active", "end")
            signal:emit("set-anactive", "start")
        else
            signal:emit("set-anactive", "end")
        end
    end
end

function UiSystem:draw()
    self.ui:draw()
end

function UiSystem:requires()
    return {}
end

function UiSystem:onAddEntity(entity)
    -- body
end

function UiSystem:onRemoveEntity(entity)
    -- body
end

return UiSystem
