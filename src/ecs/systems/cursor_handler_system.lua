---
-- cursor_handler_system.lua


local class = require "middleclass"

local CoordinateConverter = require "utils.coordinate_converter"


local CursorHandlerSystem = class("CursorHandlerSystem", System)

function CursorHandlerSystem:initialize(data)
    System.initialize(self)

    self.tile_size = 16
    self.coord_converter = CoordinateConverter({cell_size = self.tile_size})

    self.shift_x = 32
    self.shift_y = 32

    self.is_click = false

    signal:register("mouse-click", function () self.is_click = true end)
end

function CursorHandlerSystem:update(dt)
    local x, y = love.mouse.getPosition()
    local loc_x = self.coord_converter:get_local_pos(x - self.shift_x)
    local loc_y = self.coord_converter:get_local_pos(y - self.shift_y)

    for key, entity in pairs(self.targets) do
        local prev_x, prev_y = entity:get("Position"):get()

        if loc_x ~= prev_x or loc_y ~= prev_y then
            if grid:check(loc_x, loc_y) then
                entity:get("Position"):set(loc_x, loc_y)
            end
        end
    end

    if self.is_click and grid:check(loc_x, loc_y) then
        signal:emit("click-on-position", loc_x, loc_y)
    end

    self.is_click = false
end

function CursorHandlerSystem:requires()
    return {"Cursor"}
end

function CursorHandlerSystem:onAddEntity(entity)
    -- body
end

function CursorHandlerSystem:onRemoveEntity(entity)
    -- body
end

return CursorHandlerSystem
