---
-- load_scene_system.lua


local class = require "middleclass"

local EntityFactory = require "data.factories.entity_factory"


local LoadSceneSystem = class("LoadSceneSystem", System)

function LoadSceneSystem:initialize(data)
    System.initialize(self)

    self.entity_factory = EntityFactory()

    signal:register("load-scene", function () self:_load_all() end)
end

function LoadSceneSystem:update(dt)
    -- body
end

function LoadSceneSystem:requires()
    return {}
end

function LoadSceneSystem:onAddEntity(entity)
    -- body
end

function LoadSceneSystem:onRemoveEntity(entity)
    -- body
end

function LoadSceneSystem:_load_all()
    for i, j, _ in grid:iterate() do
        local cell = self.entity_factory:get_cell(i, j)

        grid:set(i, j, cell)
        engine:addEntity(cell)
    end

    local cursor = self.entity_factory:get_cursor()
    engine:addEntity(cursor)
end

return LoadSceneSystem
