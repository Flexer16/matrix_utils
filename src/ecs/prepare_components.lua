---
-- prepare_components.lua


Component.register(require "ecs.components.position")
Component.register(require "ecs.components.view")
Component.register(require "ecs.components.cell")
Component.register(require "ecs.components.cursor")
