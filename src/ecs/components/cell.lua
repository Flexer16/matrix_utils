---
-- cell.lua


local class = require "middleclass"


local Cell = class("Cell")

function Cell:initialize(data)
    self.start_position = false
    self.end_position = false
end

function Cell:is_start()
    return self.start_position
end

function Cell:is_end()
    return self.end_position
end

function Cell:set_start()
    self.start_position = true
end

function Cell:set_end()
    self.end_position = true
end

function Cell:reset()
    self.start_position = false
    self.end_position = false
end

return Cell
