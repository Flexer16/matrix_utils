---
-- main.lua


package.path = package.path .. ";lib/?/init.lua;lib/?.lua;src/?.lua"


local lovetoys = require "lovetoys"
local Signal = require "hump.signal"

local Grid = require "utils.grid"


lovetoys.initialize({
    globals = true,
    debug = true
})


require "ecs.prepare_components"


local ViewSystem = require "ecs.systems.view_system"
local LoadSceneSystem = require "ecs.systems.load_scene_system"
local CursorHandlerSystem = require "ecs.systems.cursor_handler_system"
local UiSystem = require "ecs.systems.ui_system"
local LogicSystem = require "ecs.systems.logic_system"


function love.load()
    engine = Engine()
    signal = Signal.new()
    grid = Grid(40, 33)

    local ui = UiSystem()

    engine:addSystem(LoadSceneSystem(), "update")
    engine:addSystem(CursorHandlerSystem(), "update")
    engine:addSystem(ui, "update")
    engine:addSystem(LogicSystem(), "update")

    engine:addSystem(ViewSystem(), "draw")
    engine:addSystem(ui, "draw")

    signal:emit("load-scene")
end


function love.update(dt)
    engine:update(dt)
end

function love.draw()
    engine:draw()
end

function love.mousepressed(x, y, button, istouch, presses)
    if button == 1 then
        signal:emit("mouse-click")
    end
end
